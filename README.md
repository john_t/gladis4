# Gladis 4

Forked from [Gladis](https://github.com/MicroJoe/gladis).

[![Documentation](https://docs.rs/gladis4/badge.svg)](https://docs.rs/gladis4)
[![License](https://img.shields.io/crates/l/gladis4.svg)](https://crates.io/crates/gladis4)

A Rust crate to easily import Glade-generated UI files into Rust code.

## Usage

In order to use Gladis4, you have to add the following dependencies into your
project's `Cargo.toml` file:

```toml
[dependencies]
gladis4 = "0.1.0"
```

After this is done, you can enjoy the Gladis derive!

```rust
#[derive(Gladis, Clone)]
pub struct Window {
    pub window: gtk::ApplicationWindow,
    pub label: gtk::Label,
}

impl Window {
    pub fn new() -> Self {
        Self::from_resource("/dev/null/hello_builder/window.ui").unwrap()
    }
}
```

Without Gladis, you would have to manually parse each of the Glade entries.

```rust
pub struct Window {
    pub window: gtk::ApplicationWindow,
    pub label: gtk::Label,
}

impl Window {
    pub fn new() -> Self {
        let builder = gtk::Builder::from_resource("/dev/null/hello_builder/window.ui");
        let window: gtk::ApplicationWindow = builder
            .object("window")
            .expect("Failed to find the window object");

        let label: gtk::Label = builder
            .object("label")
            .expect("Failed to find the label object");

        Self { window, label }
    }
}
```

## Relm support

This crate is compatible with [Relm4](https://github.com/AaronErhardt/Relm4), a
popular framework for writing UIs with GTK+. See the `examples/relm` directory,
and give it a shot!

## License

Licensed under either of [Apache License, Version 2.0](LICENSES/Apache-2.0.txt)
or [MIT license](LICENSES/MIT.txt) at your option.

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in this project by you, as defined in the Apache-2.0 license,
shall be dual licensed as above, without any additional terms or conditions.
