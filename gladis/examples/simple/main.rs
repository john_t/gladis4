use gio::prelude::*;
use gtk::prelude::*;

use gladis4::Gladis;

#[derive(Gladis, Clone)]
struct Ui {
    window: gtk::Window,
    button: gtk::Button,
}

fn build_ui(app: &gtk::Application) {
    let glade_src = include_str!("simple.glade");
    let ui = Ui::from_string(glade_src).unwrap();

    ui.window.set_application(Some(app));

    ui.button.connect_clicked(move |b| {
        b.set_label("clicked!");
    });

    ui.window.present();
}

fn main() {
    let application = gtk::Application::new(
        Some("com.github.MicroJoe.gladis.examples.simple"),
        Default::default(),
    );

    application.connect_activate(|app| {
        build_ui(app);
    });

    application.run();
}
